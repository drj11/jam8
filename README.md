# Jam 8

Jam 8 is a monospaced bitmap font that I created around 1999
because I got annoyed with the default console font on FreeBSD.
At the time FreeBSD console (not X) was my main home computer use.

In 2020 I made this font available as [a TrueType Font file](jam8.ttf).

It looks like this in `gnome-font-viewer`:

![Screenshot of gnome-font-viewer showing Jam 8 Regular](gfv-jam8.png "Preview of Jam 8")

Here is a 1:1 picture of the whole font:

![All the ASCII printable characters](jam8.png "Jam 8")

## Curation notes

The original materials have been lost in the mists of time.
It is likely that the original font files were created using
a text file in an editor and a C program to compile
into whatever format the FreeBSD textmode console required.

As some point I had the idea to use the font in
[PyPNG](https://github.com/drj11/pypng/).
It survived there in the
[Python source code to the `texttopng` tool](https://github.com/drj11/pypng/blob/30a5d99f1370e7ea9b9ff6229a66b9735b67a00f/code/texttopng.py#L23-L119).

In 2020 I created the [Font 8](https://git.sr.ht/~drj/font8)
bitmap to TrueType conversion system.
I used Font 8 to convert the PNG bitmap displayed here
(created using `texttopng`) to a TrueType Font file.

It would be best to consider both this font and
the Font 8 system as under development.
