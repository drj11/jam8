jam8.ttf: jam8.png control-jam8.csv
	cuttar jam8.png | vec8 - | ttf8 -dir jam8 -control control-jam8.csv -
	f8name -dir jam8 control-jam8.csv
	f8dsig -dir jam8
	assfont -dir jam8

clean:
	rm *.ttf || true
	rm *.ttx || true
	rm jam8/* || true
